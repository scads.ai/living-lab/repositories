# ScaDS.AI Git Repos

## Apurv Kulkarni
https://gitlab.hrz.tu-chemnitz.de/scads.ai/latex-template

## Daniel Obraczka
https://github.com/dobraczka/klinker
https://github.com/dobraczka/kiez
https://github.com/ScaDS/MovieGraphBenchmark
https://github.com/jonathanschuchart/eager

## Jan Frenzel
https://gitlab.hrz.tu-chemnitz.de/scads.ai/voice-assistant-to-check-appointments
https://gitlab.hrz.tu-chemnitz.de/s2817051--tu-dresden.de/froom
https://gitlab.hrz.tu-chemnitz.de/scads.ai/bigdataframeworkconfigure
https://gitlab.hrz.tu-chemnitz.de/scads.ai/performance-investigations/big-data-trace-from-history
https://gitlab.hrz.tu-chemnitz.de/scads.ai/performance-investigations/big-data-vsettings-file-creator

## Martin Grimmer
https://github.com/LID-DS/LID-DS
https://github.com/martingrimmer/IoTTest-IDS

## Martin Pippel
https://github.com/mpicbg-csbd/napari-kics

## Matti Berthold
https://github.com/DaphneOdekerken/PyArg
https://github.com/mattiberthold/ForgettingWeb
https://github.com/mattiberthold/Forgetting-via-ASP
https://sourceforge.net/p/diamond-adf/grappa/ci/master/tree/

## Norman Koch
https://gitlab.hrz.tu-chemnitz.de/scads.ai/speakscript

## [Robert Haase](https://haesleinhuepf.github.io/)

* [Bio-image Analysis Notebooks](https://github.com/haesleinhuepf/BioImageAnalysisNotebooks/)
* [NFDI4BioImage Training Materials](https://github.com/nfdi4bioimage/training/)
* [Bio-Image Analysis, Biostatistics, Programming and Machine Learning for Computational Biology Lecture in Molecular Bioengineering (Master) and Molecular Biology and Biotechnology (Bachelor) at CMCB, TU Dresden](https://github.com/BiAPoL/Bio-image_Analysis_with_Python)
* [PoL Early Career Bio-Image Analysis Training School, Dresden](https://github.com/biapol/PoL-BioImage-Analysis-TS-Early-Career-Track/)
* [PoL GPU-Accelerated Bio-Image Analysis Training School, Dresden](https://github.com/biapol/PoL-BioImage-Analysis-TS-GPU-Accelerated-Image-Analysis/)
* [Image Data Science with Python and Napari Training School, EPFL Lausanne](https://github.com/BiAPoL/Image-data-science-with-Python-and-Napari-EPFL2022)
* [Quantitative BioImage Analysis Training School, CSBD Dresden](https://github.com/BiAPoL/Quantitative_Bio_Image_Analysis_with_Python_2022)
* [CLIJ](https://github.com/clij/clij2)
  * [CLATLAB](https://github.com/clij/clatlab)
  * [clicy](https://github.com/clij/clicy)
  * [clupath](https://github.com/clij/clupath)
  * [pyclesperanto_prototype](https://github.com/clEsperanto/pyclesperanto_prototype)
  * [pyclesperanto](https://github.com/clEsperanto/pyclesperanto)
  * [clesperantoj_prototype](https://github.com/clEsperanto/clesperantoj_prototype)
* Accelerated pixel and object classification
  * [apoc](https://github.com/haesleinhuepf/apoc)
  * [napari-apoc](https://github.com/haesleinhuepf/napari-accelerated-pixel-and-object-classification)
* [stackview](https://github.com/haesleinhuepf/stackview)
* [bia-bob](https://github.com/haesleinhuepf/bia-bob)
* [devbio-napari](https://github.com/haesleinhuepf/devbio-napari)
  * [napari-segment-blobs-and-things-with-membranes](https://github.com/haesleinhuepf/napari-segment-blobs-and-things-with-membranes)
  * [napari-simpleitk-image-processing](https://github.com/haesleinhuepf/napari-simpleitk-image-processing)
  * [napari-pyclesperanto-assistant](https://github.com/clesperanto/napari_pyclesperanto_assistant)
  * [napari-cupy-image-processing](https://github.com/haesleinhuepf/napari-cupy-image-processing)
  * [napari-clusters-plotter](https://github.com/biapol/napari-clusters-plotter)
  * [napari-skimage-regionprops](https://github.com/haesleinhuepf/napari-skimage-regionprops)
  * [the-segmentation-game](https://github.com/haesleinhuepf/the-segmentation-game)
  * [napari-assistant](https://github.com/haesleinhuepf/napari-assistant)
  * [napari-process-points-and-surfaces](https://github.com/haesleinhuepf/napari-process-points-and-surfaces)
  * [napari-script-editor](https://github.com/haesleinhuepf/napari-script-editor)
  * [napari-owncloud](https://github.com/haesleinhuepf/napari-owncloud)
  * [natari](https://github.com/haesleinhuepf/natari)

## Norbert Siegmund Group
* https://github.com/AI-4-SE/Mastering-Uncertainty-in-Performance-Estimations-of-Configurable-Software-Systems
    > P4: Implementation of the Approach presented in Johannes Dorn, Sven Apel, and Norbert Siegmund. Mastering Uncertainty in Performance Estimations of Configurable Software Systems. Empirical Software Engineering
* https://github.com/AI-4-SE/CfgNet
    > CfgNet: a plugin-based framework for detecting and tracking dependencies among configuration options across the used technology stack of a software project. 
* https://github.com/AI-4-SE/TwinsOrFalseFriends
    > Replication Package for Energy & Performance Study “Twins or False Friends”
* https://zenodo.org/records/7520777
    > Replication Package for “Socio-Technical Anti-Patterns in Building ML-enabled Software” 
* https://zenodo.org/records/7745740
    > Replication Package for “Exploring Hyperparameter Usage and Tuning in Machine Learning Research”


## Webis Group (Martin Potthast et al.)

* https://github.com/webis-de/ECIR-19
* https://github.com/webis-de/ACL-19
* https://github.com/webis-de/ECIR-20
* https://github.com/webis-de/sigir20-sampling-bias-due-to-near-duplicates-in-learning-to-rank
* https://github.com/webis-de/acl20-personal-characteristics-predicting-persuasiveness
* https://github.com/webis-de/acl20-target-inference-in-conclusion-generation
* https://github.com/webis-de/acl20-crawling-mailing-lists
* https://github.com/webis-de/acl20-efficient-argument-quality-annotation
* https://github.com/webis-de/CIKM-20
* https://github.com/webis-de/CIKM-20
* https://github.com/causenet-org/CIKM-20
* https://github.com/webis-de/CIKM-20
* https://github.com/webis-de/COLING-20
* https://github.com/webis-de/ecir21-an-empirical-comparison-of-web-page-segmentation-algorithms
* https://github.com/webis-de/SIGIR-21
* https://github.com/webis-de/SIGIR-21
* https://github.com/webis-de/SIGIR-21
* https://github.com/webis-de/argmining19-same-side-classification
* https://github.com/webis-de/ACL-21
* https://github.com/webis-de/ACL-21
* https://github.com/webis-de/ACL-21
* https://github.com/webis-de/ml4cd-21
* https://github.com/webis-de/EMNLP-21
* https://github.com/webis-de/summary-explorer
* https://github.com/webis-de/EMNLP-21
* https://github.com/webis-de/ArgMining-21
* https://github.com/heinrichreimer/modern-talking/
* https://github.com/webis-de/amoc-21
* https://github.com/webis-de/WSDM-22
* https://github.com/webis-de/ecir22-anchor-text
* https://github.com/webis-de/acl-22
* https://github.com/webis-de/acl-22
* https://github.com/webis-de/in2writing22-language-models-as-context-sensitive-word-search-engines
* https://github.com/webis-de/ICTIR-22
* https://github.com/webis-de/tpdl22-visual-web-archive-quality-assessment
* https://github.com/heinrichreimer/grimjack
* https://github.com/webis-de/COLING-22
* https://github.com/webis-de/COLING-22
* https://github.com/chatnoir-eu/chatnoir-warc-dl
* https://github.com/webis-de/SPIRE-22
* https://github.com/webis-de/AACL-22
* https://github.com/webis-de/summary-workbench
* https://github.com/webis-de/scidata22-stereo-scientific-text-reuse
* https://github.com/webis-de/ECIR-23
* https://github.com/tira-io/tira
* https://github.com/webis-de/small-text
* https://github.com/webis-de/EACL-23
* https://github.com/webis-de/EACL-23
* https://github.com/webis-de/JCDL-23
* https://github.com/webis-de/JCDL-23
* https://github.com/webis-de/sigir23-stance-detection-in-image-retrieval-for-argumentation
* https://github.com/tira-io/ir-experiment-platform
* https://github.com/webis-de/archive-query-log
* https://github.com/hscells/pybool_ir
* https://github.com/webis-de/SIGIR-23
* https://github.com/MattiWe/acl23-trigger-warning-assignment
* https://github.com/webis-de/arxiv23-prompt-embedding-manipulation
* https://github.com/google/BIG-bench/tree/main/bigbench/benchmark_tasks/authorship_verification